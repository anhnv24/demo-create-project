'use strict';


var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// khoi tạo schema taskSchema
var CourseSchema = new Schema({
    title: {
        type: String,
        required: [true, 'Kindly enter the name of the task']
    },
    imageUrl: {
        type: String
    },
    duration: {
        type: String
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    des: {
        type: [{
            type: String  
        }],
    },
    requirement: {
        type: [{
            type: String  
        }],
    },
    route: {
        type:[{
            
        }]
    }
});
// nếu ko sử dụng index sẽ phải duyệt tất cả
//  document trong MongoDB thỏa mãn điều kiện truy vấn
//dùng để giới hạn số lượng document phải kiểm tra
CourseSchema.index({ '$**': "text" }) //Dùng để search

// chuyen Schema sang dạng Model de Controller ssu dung xu ly tuong tac voi DB
module.exports = mongoose.model('Course', CourseSchema);