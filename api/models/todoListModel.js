'use strict';


var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// khoi tạo schema taskSchema
var TaskSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Kindly enter the name of the task']
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  status: {
    type: [{
      type: String,
      enum: ['xong', 'chua xong']
    }],
    default: ['chua xong']
  }
});
// nếu ko sử dụng index sẽ phải duyệt tất cả
//  document trong MongoDB thỏa mãn điều kiện truy vấn
//dùng để giới hạn số lượng document phải kiểm tra
TaskSchema.index( { '$**': "text" } ) //Dùng để search

// chuyen Schema sang dạng Model de Controller ssu dung xu ly tuong tac voi DB
module.exports = mongoose.model('Tasks', TaskSchema);