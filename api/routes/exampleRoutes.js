module.exports = function (app) {
    var todoList = require('../controllers/todoListController');

    //todoList route
    app.route('/tasks1')
    .get(todoList.list_all_tasks)
    .post(todoList.create_a_tasks);

    app.route('/tasks1/:taskId')
    .get(todoList.read_a_tasks)
    .put(todoList.update_a_tasks)
    .delete(todoList.delete_a_tasks);

};