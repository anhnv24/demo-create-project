var express = require('express'),
  app = express(),
  port = process.env.PORT || 3001,
  cors = require('cors')
  // 
  mongoose = require('mongoose'),
  // 
  bodyParser = require('body-parser');
//  gọi models task để sd
// Tasks = require('./api/models/todoListModel');
Models = require('./api/models');
// gọi tới thư viện express
fileUpload = require('express-fileupload');
path = require('path');
// kết nối mongoose
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/eledevo_demo',
  { useNewUrlParser: true }).then(() => {
    console.log("Connected !!!")
  }).catch(err => {
    console.log(err);
  });
//   
//socket
// var server = require('http').Server(app);
// var io = require('socket.io')(server);

// server.listen(3002);
// io.on('connection',function(socket){
//   io.sockets.emit("user_online", socket.id + ' is connected');
//   socket.on('message',function(msg){
//     socket.broadcast.emit("re_message", socket.id +": "+ msg);
//   });
//   socket.on('disconnect',function(msg){
//     socket.broadcast.emit("re_message", socket.id +": is disconnected");
//   });
// });
//
 
app.use(cors())
app.use(bodyParser.json());
// upload ảnh
app.use(express.static(path.join(__dirname, 'public/images')));
app.use(fileUpload({
  useTempFiles: true,
  tempFileDir: '/tmp/'
}));
// 
var routes = require('./api/routes');
routes(app);

app.use(function (req, res) {
  res.status(404).send({ url: req.originalUrl + ' not found' })
});

app.listen(port);

console.log("todo list RESTFul API server started on", port);